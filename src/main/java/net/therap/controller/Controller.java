package net.therap.controller;

import net.therap.dao.ItemDao;
import net.therap.dao.MenuDao;
import net.therap.domain.Item;
import net.therap.domain.Menu;
import net.therap.helper.Constants;
import net.therap.helper.HibernateUtil;
import net.therap.view.Printer;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.util.*;

/**
 * @author shadman
 * @since 11/8/17
 */
public class Controller implements Serializable {

    public static final long serialVersionUID = 1L;

    private SessionFactory sessionFactory;
    private Printer printer;
    private ItemDao itemDao;
    private MenuDao menuDao;

    public Controller() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
        this.printer = new Printer();
        this.itemDao = new ItemDao(this.sessionFactory);
        this.menuDao = new MenuDao(this.sessionFactory);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public Printer getPrinter() {
        return printer;
    }

    public void setPrinter(Printer printer) {
        this.printer = printer;
    }

    public ItemDao getItemDao() {
        return itemDao;
    }

    public void setItemDao(ItemDao itemDao) {
        this.itemDao = itemDao;
    }

    public MenuDao getMenuDao() {
        return menuDao;
    }

    public void setMenuDao(MenuDao menuDao) {
        this.menuDao = menuDao;
    }

    public void viewItems() {
        List<Item> itemList = itemDao.getAllItems();
        printer.viewItems(itemList);
    }

    public void updateItem() {
        int idOfItemToBeUpdated = printer.getIdOfItemToBeUpdated();
        List<Object> updatedItemAttributes = printer.getNewItemAttributes();
        itemDao.updateItem(idOfItemToBeUpdated, updatedItemAttributes);
    }

    public void deleteItem() {
        int idOfItemToBeDeleted = printer.getIdOfItemToBeUpdated();
        Item item = itemDao.getItemFromId(idOfItemToBeDeleted);
        if (item != null) {
            itemDao.deleteItem(item);
        }
    }

    public void createItem() {
        List<Object> newItemAttributes = printer.getNewItemAttributes();
        Item item = new Item((String) newItemAttributes.get(0));
        itemDao.addItem(item);
    }

    public Set<Item> getItemSetFromUser() {
        viewItems();
        Map<Integer, Boolean> existingItems = new HashMap<>();
        List<Item> itemList = itemDao.getAllItems();
        for (Item item : itemList) {
            existingItems.put(item.getId(), true);
        }
        List<Integer> idList = printer.getCreateMenuItems(existingItems);
        Set<Item> userChosenItemSet = new LinkedHashSet<>();
        for (Integer id : idList) {
            userChosenItemSet.add(itemDao.getItemFromId(id));
        }
        return userChosenItemSet;
    }

    public void updateMenu() {
        int updateMenuDay = printer.getUpdateMenuDay();
        int updateMenuServingType = printer.getUpdateMenuServingType();
        Menu oldMenu = menuDao.getMenuFromDayAndServingType(updateMenuDay, updateMenuServingType);
        Set<Item> updateMenuItemSet = getItemSetFromUser();
        Menu newMenu = new Menu(updateMenuDay, updateMenuItemSet, updateMenuServingType);
        menuDao.updateMenu(oldMenu, newMenu);
    }

    public void deleteMenu() {
        int deleteMenuDay = printer.getUpdateMenuDay();
        int deleteMenuServingType = printer.getUpdateMenuServingType();
        Menu menu = menuDao.getMenuFromDayAndServingType(deleteMenuDay, deleteMenuServingType);
        menuDao.deleteMenu(menu);
    }

    public void createMenu() {
        int createMenuDay = printer.getCreateMenuDay();
        int createMenuServingType = printer.getCreateMenuServingType();
        Set<Item> userChosenItemSet = getItemSetFromUser();
        Menu menu = new Menu(createMenuDay, userChosenItemSet, createMenuServingType);
        menuDao.addMenu(menu);
    }

    public void communicate() {
        int whatToDo = this.printer.getWhatToDo();
        switch (whatToDo) {
            case Constants.VIEW:
                int whatToView = this.printer.getWhatToView();
                switch (whatToView) {
                    case Constants.VIEW_MENU:
                        int whichMenuToView = printer.getWhichMenuToView();
                        switch (whichMenuToView) {
                            case Constants.VIEW_BREAKFAST_MENU:
                            case Constants.VIEW_LUNCH_MENU:
                                List<Menu> menuList = menuDao.getWeekLyMenu(whichMenuToView);
                                printer.viewMenu(menuList);
                                break;
                        }
                        break;
                    case Constants.VIEW_ITEMS:
                        viewItems();
                        break;
                }
                break;
            case Constants.UPDATE:
                int updateOperation = this.printer.getUpdateOperation();
                switch (updateOperation) {
                    case Constants.CREATE_ITEM:
                        createItem();
                        break;
                    case Constants.UPDATE_ITEM:
                        viewItems();
                        updateItem();
                        break;
                    case Constants.DELETE_ITEM:
                        viewItems();
                        deleteItem();
                        break;
                    case Constants.CREATE_MENU:
                        createMenu();
                        break;
                    case Constants.UPDATE_MENU:
                        updateMenu();
                        break;
                    case Constants.DELETE_MENU:
                        deleteMenu();
                        break;
                }
                break;
        }
        this.sessionFactory.close();
    }
}
