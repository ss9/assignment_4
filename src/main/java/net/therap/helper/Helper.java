package net.therap.helper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shadman
 * @since 11/12/17
 */
public class Helper implements Serializable {

    public static final long serialVersionUID = 1L;

    public static final Map<Integer, String> INT_TO_DAY;
    public static final Map<Integer, String> INT_TO_SERVING_TIME;

    static {
        INT_TO_DAY = new HashMap<>();
        INT_TO_DAY.put(1, "Sunday");
        INT_TO_DAY.put(2, "Monday");
        INT_TO_DAY.put(3, "Tuesday");
        INT_TO_DAY.put(4, "Wednesday");
        INT_TO_DAY.put(5, "Thursday");
        INT_TO_DAY.put(6, "Friday");
        INT_TO_DAY.put(7, "Saturday");

        INT_TO_SERVING_TIME = new HashMap<>();
        INT_TO_SERVING_TIME.put(1, "Breakfast");
        INT_TO_SERVING_TIME.put(2, "Lunch");
    }
}
