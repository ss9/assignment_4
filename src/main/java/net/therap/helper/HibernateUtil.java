package net.therap.helper;

import net.therap.domain.Item;
import net.therap.domain.Menu;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.io.Serializable;

/**
 * @author shadman
 * @since 11/15/17
 */
public class HibernateUtil implements Serializable {

    public static final long serialVersionUID = 1L;

    public static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration();
        configuration.configure().addAnnotatedClass(Item.class);
        configuration.configure().addAnnotatedClass(Menu.class);
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
                applySettings(configuration.getProperties()).build();
        return configuration.buildSessionFactory(serviceRegistry);
    }
}
