package net.therap.dao;

import net.therap.domain.Item;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.util.List;

/**
 * @author shadman
 * @since 11/12/17
 */
public class ItemDao implements Serializable {

    public static final long serialVersionUID = 1L;

    private SessionFactory sessionFactory;

    public ItemDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Item> getAllItems() {
        List<Item> itemList = null;
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery("FROM Item");
            itemList = query.list();
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return itemList;
    }

    public Item getItemFromId(int id) {
        Item item = null;
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            item = (Item) session.get(Item.class, id);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return item;
    }

    public void addItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            session.save(item);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void deleteItem(Item item) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            session.delete(item);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void updateItem(int idOfItemToBeUpdated, List<Object> updatedItemAttributes) {
        Item item = getItemFromId(idOfItemToBeUpdated);
        if (item == null) {
            return;
        }
        Session session = sessionFactory.getCurrentSession();
        try {
            session.getTransaction().begin();
            item.setName((String) updatedItemAttributes.get(0));
            session.update(item);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }
}
