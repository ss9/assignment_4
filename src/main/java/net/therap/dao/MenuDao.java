package net.therap.dao;

import net.therap.domain.Menu;
import net.therap.domain.MenuPrimaryKey;
import net.therap.helper.Helper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author shadman
 * @since 11/12/17
 */
public class MenuDao implements Serializable {

    public static final long serialVersionUID = 1L;

    private SessionFactory sessionFactory;

    public MenuDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public boolean updateMenu(Menu oldMenu, Menu newMenu) {
        deleteMenu(oldMenu);
        addMenu(newMenu);
        return true;
    }

    public void deleteMenu(Menu menu) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            session.delete(menu);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public void addMenu(Menu menu) {
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            session.save(menu);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
    }

    public Menu getMenuFromDayAndServingType(int day, int servingType) {
        Menu menu = null;
        Session session = sessionFactory.getCurrentSession();
        try {
            session.beginTransaction();
            MenuPrimaryKey menuPrimaryKey = new MenuPrimaryKey(day, servingType);
            menu = (Menu) session.get(Menu.class, menuPrimaryKey);
            session.getTransaction().commit();
        } catch (RuntimeException e) {
            session.getTransaction().rollback();
        }
        return menu;
    }

    public List<Menu> getWeekLyMenu(int servingType) {
        List<Menu> menuList = new ArrayList<>();
        for (int key : Helper.INT_TO_DAY.keySet()) {
            menuList.add(getMenuFromDayAndServingType(key, servingType));
        }
        return menuList;
    }
}
