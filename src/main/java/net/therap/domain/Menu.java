package net.therap.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * @author shadman
 * @since 11/15/17
 */

@Entity
@IdClass(MenuPrimaryKey.class)
@Table(name = "menu")
public class Menu implements Serializable {

    public static final long serialVersionUID = 1L;

    @Column(nullable = false, length = 5)
    @Id
    private int day;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Item> itemSet = new HashSet<>();

    @Column(name = "serving_type", nullable = false, length = 5)
    @Id
    private int servingType;

    public Menu() {

    }

    public Menu(int day, Set<Item> itemSet, int servingType) {
        this.day = day;
        this.itemSet = itemSet;
        this.servingType = servingType;
    }

    public Set<Item> getItemSet() {
        return itemSet;
    }

    public void setItemSet(Set<Item> itemSet) {
        this.itemSet = itemSet;
    }

    public int getServingType() {
        return servingType;
    }

    public void setServingType(int servingType) {
        this.servingType = servingType;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}
